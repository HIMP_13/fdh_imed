# HADOOP DATA FLOW #


This project explores some Hadoop environment tools and data processing throughout itself. Starting with data ingestion from a Relational Database with Sqoop or with a Streaming data source with Kafka and its processing and exploitation with Spark or Hive. 


### CONFIGURING ENVIRONMET TOOLS

Speaking general for installation follow this process:

- Getting the .tar files from Apache.org website.
- Editing configuration files.
- HDFS installation needs openssh.
- Each tool has its particular configuration.


### DATA PROCESSING

Process starts with Sqoop ingestion from a Relational Database in PostgreSQL to HDFS. After that, Spark-Pyspark process ingested data and creates a database in Hive for exploiting. 
In other hand, Kafka transfers data from a Streaming local data source working with Spark-Scala to HDFS where real-time tables are created in Hive.