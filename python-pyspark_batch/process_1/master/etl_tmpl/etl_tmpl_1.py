import sys

from pyspark.sql import SparkSession
from pyspark.sql.types import *


class EtlTmpl_1:

    def spark_build(self, SparkSession):
        return SparkSession.builder.enableHiveSupport().getOrCreate()

    def read_write_csv(self, spark):
        self.load_ciudades = spark.read.format("csv").option("sep", ";").option("header", "true").load(
            "file:///home/isamed/training_materials/tablas_csv/tabla_ciudades.csv")
        self.load_ciudades.repartition(1).write.option("header", "true").option("sep", ";").csv(
            "hdfs://localhost:9000/inputs/ciudades_coord_test13")
        self.db_fdh = spark.sql("USE  proyecto_fdh")
        self.tabla_ciudades = spark.sql(
            "CREATE TABLE ciudades_coord_bis3 (ciudad STRING, latitud STRING, longitud STRING) "
            + "ROW FORMAT DELIMITED "
            + "FIELDS TERMINATED BY ';' "
            + "LOCATION '/inputs/ciudades_coord' ")

        return None

    def read_write_csv_2(self, spark):
        self.load_ciudades = spark.read.format("csv").option("sep", ";").option("header", "true").load(
            "file:///home/isamed/training_materials/tablas_csv/tabla_ciudades.csv")
        self.load_ciudades.repartition(1).write.option("header", "true").option("sep", ";").csv(
            "hdfs://localhost:9000/inputs/ciudades_coord_test14")
        self.db_fdh = spark.sql("USE  proyecto_fdh")
        self.nombre_tabla = "ciudades_cord_bis4"
        self.tabla_ciudades = spark.sql(
            "CREATE TABLE " + self.nombre_tabla + "(ciudad STRING, latitud STRING, longitud STRING) "
            + "ROW FORMAT DELIMITED "
            + "FIELDS TERMINATED BY ';' "
            + "LOCATION '/inputs/ciudades_coord' ")

        return None

    def process_1(self, n1: str, n2: str, n3: str, n4: str, spark):

        def controler():

            if n1.__contains__("ingest_1"):

                read_write_csv_3()

            else:
                print("ingest_1 no ejecutado")

            if n2.__contains__("ingest_2"):

                read_write_csv_4()

            else:
                print("ingest_2 no ejecutado")

            if n3.__contains__("applySchema_json"):

                applySchema_json()

            else:
                print("")

            if n4.__contains__("accounts_table"):

                accounts_table()

            else:
                print("")

        def read_write_csv_3():
            self.load_ciudades = spark.read.format("csv").option("sep", ";").option("header", "true").load(
                "file:///home/isamed/training_materials/tablas_csv/tabla_ciudades.csv")
            self.load_ciudades.repartition(1).write.option("header", "true").option("sep", ";").csv(
                "hdfs://localhost:9000/inputs/ciudades_coord_test24")
            self.db_fdh = spark.sql("USE  proyecto_fdh")
            self.nombre_tabla = "ciudades_cord_bis8"
            self.tabla_ciudades = spark.sql(
                "CREATE TABLE " + self.nombre_tabla + "(ciudad STRING, latitud STRING, longitud STRING) "
                + "ROW FORMAT DELIMITED "
                + "FIELDS TERMINATED BY ';' "
                + "LOCATION '/inputs/ciudades_coord' ")

            return None

        def read_write_csv_4():
            self.load_ciudades = spark.read.format("csv").option("sep", ";").option("header", "true").load(
                "file:///home/isamed/training_materials/tablas_csv/tabla_ciudades.csv")
            self.load_ciudades.repartition(1).write.option("header", "true").option("sep", ";").csv(
                "hdfs://localhost:9000/inputs/ciudades_coord_test24")
            self.db_fdh = spark.sql("USE  proyecto_fdh")
            self.nombre_tabla = "ciudades_cord_bis9"
            self.tabla_ciudades = spark.sql(
                "CREATE TABLE " + self.nombre_tabla + "(ciudad STRING, latitud STRING, longitud STRING) "
                + "ROW FORMAT DELIMITED "
                + "FIELDS TERMINATED BY ';' "
                + "LOCATION '/inputs/ciudades_coord' ")

            return None

        def applySchema_json():

            self.okType = [
                StructField("devnum", LongType()),
                StructField("make", StringType()),
                StructField("model", StringType()),
                StructField("release_dt", TimestampType()),
                StructField("dev_type", StringType())]
            self.okSchema = StructType(self.okType)
            self.dev = spark.read.schema(self.okSchema).json("hdfs://localhost:9000/loudacre/devices.json")
            self.db = spark.sql("USE proyecto_fdh")
            self.dev.repartition(1).write.saveAsTable("dev_sch_json_ok")
            self.dev.repartition(1).write.save("hdfs://localhost:9000/loudacre/devices_parquet")

            return None

        def accounts_table():

            self.okType = [
                StructField("acct_num", IntegerType()),
                StructField("acct_create_dt", TimestampType()),
                StructField("acct_close_dt", TimestampType()),
                StructField("first_name", StringType()),
                StructField("last_name", StringType()),
                StructField("address", StringType()),
                StructField("city", StringType()),
                StructField("state", StringType()),
                StructField("zipcode", StringType()),
                StructField("phone_number", StringType()),
                StructField("created", TimestampType()),
                StructField("modified", TimestampType())]
            self.okSchema = StructType(self.okType)
            self.load_accounts = spark.read.schema(self.okSchema).format("csv").option("sep", ",").option("header", "true").load("file:///home/isamed/training_materials/devsh/data/accounts.csv")
            self.db = spark.sql("USE proyecto_fdh")
            self.load_accounts.repartition(1).write.saveAsTable("accounts")
            return None

        controler()