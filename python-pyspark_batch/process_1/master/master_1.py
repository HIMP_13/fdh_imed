import sys
from process_1.master.etl_tmpl.etl_tmpl_1 import EtlTmpl_1
from pyspark.sql import SparkSession


def main():
    spark_init_1 = EtlTmpl_1()
    spark = spark_init_1.spark_build(SparkSession)
    spark.sparkContext.setLogLevel("WARN")
    # spark_init_1.spark_build(SparkSession)
    job_1 = EtlTmpl_1()
    job_2 = EtlTmpl_1()

    ##spark_arg = sys.argv[1]

    # job_1.read_write_csv(spark_arg)
    # job_2.read_write_csv_2(spark_arg)

    arg_1 = sys.argv[1]
    arg_2 = sys.argv[2]
    arg_3 = sys.argv[3]
    arg_4 = sys.argv[4]
    job_3 = EtlTmpl_1()
    job_3.process_1(arg_1, arg_2, arg_3, arg_4, spark)


if __name__ == "__main__":
    main()