package batch_1

import org.apache.spark.sql.SparkSession

object executor1 {

  def main(args: Array[String]) {

    val spark = SparkSession.builder.enableHiveSupport.getOrCreate()
    spark.sparkContext.setLogLevel("WARN")

    //val process1 = new ingest_1(args(0), args(1), args(2), spark)
    val process2 = new ingest_1(args(0), args(1), args(2), spark)

    //val name = args(0)
    val ar1_ing_1 = args(0)
    val ar2_ing_2 = args(1)

    //val flag_control1 = process1.ing_1(name)
    val flag_control2 = process2.ing_2(ar1_ing_1, ar2_ing_2)

  }

}

