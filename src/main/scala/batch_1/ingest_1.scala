package batch_1


import org.apache.spark.sql.SparkSession

import org.apache.spark.SparkContext

import org.apache.spark.sql.functions.{format_string, _}

import org.apache.spark.sql.functions._

import org.apache.spark.sql.types._

import org.apache.spark.rdd.RDD

import org.apache.spark.sql.Row

class ingest_1 (arg_0: String, arg_1: String, arg_2: String, spark_arg:SparkSession) {

  val at_arg_0: String = arg_0
  val at_arg_1: String = arg_1
  val at_arg_2: String = arg_2
  val spark: SparkSession = spark_arg

  spark.sparkContext.setLogLevel("WARN")

  val sparkContext: SparkContext = spark.sparkContext

  import spark.implicits._

  import org.apache.spark.sql.types._

  import org.apache.spark.rdd.RDD

  import org.apache.spark.sql.Row

  def ing_1(name: String): Unit = {

    val load_ciudades = spark.read.format("csv").option("sep", ";").option("header", "true").load("file:///home/isamed/training_materials/tablas_csv/tabla_ciudades.csv")
    load_ciudades.repartition(1).write.option("header", "true").option("sep", ";").csv("hdfs://localhost:9000/inputs/ciudades_coord_test17")
    val db_fdh = spark.sql("USE  proyecto_fdh")
    val name_tabla = name
    val tabla_ciudades = spark.sql("CREATE TABLE " + name_tabla + "(ciudad STRING, latitud STRING, longitud STRING) "
      + "ROW FORMAT DELIMITED "
      + "FIELDS TERMINATED BY ';' "
      + "LOCATION '/inputs/ciudades_coord' ")
    //tabla_ciudades.show(5)
  }

  def ing_2(n1: String, n2: String): Unit = {

    def controler(): Unit = {

      if (n1.contains("ingest_1")) ingest_sub1()
      else println("ingest_1 no ejecutado")
      if (n2.contains("ingest_2")) ingest_sub2()
      else println("ingest_2 no ejecutado")

    }

    def ingest_sub1(): Unit = {

      val load_ciudades = spark.read.format("csv").option("sep", ";").option("header", "true").load("file:///home/isamed/training_materials/tablas_csv/tabla_ciudades.csv")
      load_ciudades.repartition(1).write.option("header", "true").option("sep", ";").csv("hdfs://localhost:9000/inputs/ciudades_coord_test19")
      val db_fdh = spark.sql("USE  proyecto_fdh")
      val name_tabla = "ciudades_coord_sc_2"
      val tabla_ciudades = spark.sql("CREATE TABLE " + name_tabla + "(ciudad STRING, latitud STRING, longitud STRING) "
        + "ROW FORMAT DELIMITED "
        + "FIELDS TERMINATED BY ';' "
        + "LOCATION '/inputs/ciudades_coord' ")

    }

    def ingest_sub2(): Unit = {

      val load_ciudades = spark.read.format("csv").option("sep", ";").option("header", "true").load("file:///home/isamed/training_materials/tablas_csv/tabla_ciudades.csv")
      load_ciudades.repartition(1).write.option("header", "true").option("sep", ";").csv("hdfs://localhost:9000/inputs/ciudades_coord_test20")
      val db_fdh = spark.sql("USE  proyecto_fdh")
      val name_tabla = "ciudades_coord_sc_3"
      val tabla_ciudades = spark.sql("CREATE TABLE " + name_tabla + "(ciudad STRING, latitud STRING, longitud STRING) "
        + "ROW FORMAT DELIMITED "
        + "FIELDS TERMINATED BY ';' "
        + "LOCATION '/inputs/ciudades_coord' ")

    }

    controler()

  }
}

