package streaming
//import org.apache.spark.streaming.kafka._
//import kafka.serializer.StringDecoder
import org.apache.log4j._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{StringType, StructField, StructType}

object consumer_sp {
  def main(args: Array[String]) {

    val spark = SparkSession.builder.getOrCreate()
    Logger.getLogger(("""org""")).setLevel(Level.ERROR)

    val kafkaStream = spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", "ubuntu:9092")
      .option("subscribe", "weblogs_test1")
      .load()

    import spark.implicits._

    val kafkaToString = kafkaStream.selectExpr("CAST(value AS STRING)")

    val mySchema = StructType(Array(
      StructField("make",StringType,true)))

    val kafkaDS = kafkaToString.select(from_json($"value", mySchema).as("esquema"))

    val kafkaIn = kafkaDS.selectExpr("esquema.make") //,"esquema.tipoevento","esquema.fecha","esquema.location")

    val query = kafkaIn.writeStream.format(("csv")).option("format", "append").option("path", "hdfs://localhost:9000/inputs/topic_weblogs_testq1ls").option("checkpointLocation", "" +
      "" +
      "checkpoint").outputMode("append").start()

    query.awaitTermination()
  }
}